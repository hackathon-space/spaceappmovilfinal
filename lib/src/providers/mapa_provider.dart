import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:spaceapp/src/models/dash_model.dart';

import 'package:spaceapp/src/models/mapas_model.dart';

class MapaProvider {
  final String _url = 'https://fir-space-6a979-default-rtdb.firebaseio.com/';

  Future<List<MapaDataModel>> cargarMapa() async {
    final url = '$_url/Basegeneral.json';
    final resp = await http.get(Uri.parse(url));

    final Map<String, dynamic> decodedData = json.decode(resp.body);
    final List<MapaDataModel> mapas = [];

    if (decodedData == null) return [];

    decodedData.forEach((id, mapa) {
      final mapasTemp = MapaDataModel.fromJson(mapa);
      mapasTemp.id = id;

      mapas.add(mapasTemp);
    });

    //print( equipo[0].id );
    print("mapas");
    print(mapas);

    return mapas;
  }

  DataDashboard() async {
    final url = '$_url/Basegeneral.json';
    final resp = await http.get(Uri.parse(url));

    final Map<String, dynamic> decodedData = json.decode(resp.body);
    // final List<DashboardDataModel> dataDash = [];
    // final dataDash = DataDashboard()
    final Map<String, dynamic> dataDashTemp = {
      "bolsa": 0,
      "botella": 0,
      "carton": 0,
      "cuaderno": 0,
      "mascarilla": 0
    };
    decodedData.forEach((id, mapa) {
      final mapasTemp = MapaDataModel.fromJson(mapa);
      dataDashTemp['bolsa'] = dataDashTemp['bolsa'] + mapasTemp.bolsa;
      dataDashTemp['botella'] = dataDashTemp['botella'] + mapasTemp.botella;
      dataDashTemp['carton'] = dataDashTemp['carton'] + mapasTemp.carton;
      dataDashTemp['cuaderno'] = dataDashTemp['cuaderno'] + mapasTemp.cuaderno;
      dataDashTemp['mascarilla'] =
          dataDashTemp['mascarilla'] + mapasTemp.mascarilla;
    });
    // dataDash.add(dataDashTemp['bolsa']);
    print('lista');
    print(dataDashTemp);

    return dataDashTemp;
  }
}
