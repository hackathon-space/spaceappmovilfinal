import 'dart:async';
// import 'dart:ffi';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:spaceapp/src/widgets/menu_widget.dart';
import 'package:spaceapp/src/models/mapas_model.dart';
import 'package:spaceapp/src/providers/mapa_provider.dart';

class MapaPage extends StatefulWidget {
  @override
  MapaPageState createState() => MapaPageState();
}

class MapaPageState extends State<MapaPage> {
  final mapasProvider = new MapaProvider();

  Completer<GoogleMapController> _controller = Completer();
  MapType tipomapa = MapType.normal;

  @override
  void initState() {
    super.initState();
  }

  double zoomVal = 5.0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        /*       leading: IconButton(
          iconSize:25,
          icon: const Icon(Icons.arrow_back_rounded),
          onPressed: () {Navigator.of(context).pop(); },
        ) ,*/
        title: Center(child: Text("Location")),
        actions: <Widget>[
          IconButton(
              icon: Icon(FontAwesomeIcons.map),
              onPressed: () {
                _cambiomapa1();
              }),
          IconButton(
              icon: Icon(FontAwesomeIcons.mapMarked),
              onPressed: () {
                _cambiomapa2();
              }),
        ],
      ),
      drawer: MenuWidget(),
      body: FutureBuilder(
        future: mapasProvider.cargarMapa(),
        builder: (BuildContext context,
            AsyncSnapshot<List<MapaDataModel>> snapshot) {
          if (snapshot.hasData) {
            final mapas = snapshot.data;

            Set<Marker> markers = new Set<Marker>();
            for (MapaDataModel mapa in mapas) {
              markers.add(new Marker(
                markerId: MarkerId(mapa.nombre),
                position: LatLng(mapa.latitud, mapa.longitud),
                infoWindow: InfoWindow(
                    title: mapa.nombre,
                    snippet: '${mapa.botella} Botellas Disponibles '),
                icon: BitmapDescriptor.defaultMarkerWithHue(
                  BitmapDescriptor.hueViolet,
                ),
              ));
            }

            return Stack(
              children: <Widget>[
                _buildGoogleMap(context, markers),
                _zoomminusfunction(),
                _zoomplusfunction(),
                _buildContainer(mapas),
              ],
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }

  Widget _zoomminusfunction() {
    return Align(
      alignment: Alignment.topLeft,
      child: IconButton(
          icon: Icon(FontAwesomeIcons.searchMinus, color: Color(0xff00578b)),
          onPressed: () {
            zoomVal--;
            _minus(zoomVal);
          }),
    );
  }

  Widget _zoomplusfunction() {
    return Align(
      alignment: Alignment.topRight,
      child: IconButton(
          icon: Icon(FontAwesomeIcons.searchPlus, color: Color(0xff00578b)),
          onPressed: () {
            zoomVal++;
            _plus(zoomVal);
          }),
    );
  }

  Future<void> _minus(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: LatLng(-11.0463731, -77.042754), zoom: zoomVal)));
  }

  Future<void> _plus(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: LatLng(-11.0463731, -77.042754), zoom: zoomVal)));
  }

  void _cambiomapa1() {
    setState(() => tipomapa = MapType.satellite);
  }

  void _cambiomapa2() {
    setState(() => tipomapa = MapType.normal);
  }

  Widget _buildContainer(List<MapaDataModel> mapas) {
    final ScrollController scrollController = ScrollController();

    return SizedBox(
      //height: _screeSize.height-105,
      child: Align(
        alignment: Alignment.bottomLeft,
        child: Container(
            margin: EdgeInsets.symmetric(vertical: 20.0),
            height: 150.0,
            child: Scrollbar(
              isAlwaysShown: true,
              controller: scrollController,
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  controller: scrollController,
                  itemCount: mapas.length,
                  itemBuilder: (context, i) {
                    return Row(
                      children: [
                        SizedBox(width: 10.0),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: _boxes(context, mapas[i]),
                        ),
                      ],
                    );
                  }),
            )),
      ),
    );
  }

//,String _image, double lat,double long,String restaurantName
//     "https://media-cdn.tripadvisor.com/media/photo-s/18/f8/91/42/torre-torre.jpg",
  //       40.738380, -73.988426,"Gramercy Tavern"
  Widget _boxes(BuildContext context, MapaDataModel mapas) {
    return GestureDetector(
      onTap: () {
        _gotoLocation(mapas.latitud, mapas.longitud);
      },
      child: Container(
        child: new FittedBox(
          child: Material(
              color: Colors.white,
              elevation: 14.0,
              borderRadius: BorderRadius.circular(24.0),
              shadowColor: Color(0x802196F3),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    width: 190,
                    height: 230,
                    child: ClipRRect(
                      borderRadius: new BorderRadius.circular(24.0),
                      child: Image(
                          fit: BoxFit.fill, image: NetworkImage(mapas.imagen)),
                      // image: NetworkImage( "https://media-cdn.tripadvisor.com/media/photo-s/18/f8/91/42/torre-torre.jpg")),
                    ),
                  ),
                  Container(
                    width: 300,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: myDetailsContainer1(mapas),
                    ),
                  ),
                ],
              )),
        ),
      ),
    );
  }

  Widget myDetailsContainer1(MapaDataModel mapas) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Center(
          child: Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Container(
                child: Text(
              mapas.nombre,
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Color(0xff6200ee),
                  fontSize: 24.0,
                  fontWeight: FontWeight.bold),
            )),
          ),
        ),
        SizedBox(height: 10.0),
        Container(
            child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            SizedBox(width: 30.0),
            Container(
                child: Text(
              '${mapas.cuaderno}',
              style: TextStyle(
                color: Colors.black54,
                fontSize: 22.0,
              ),
            )),
            Container(
              child: Icon(
                Icons.book,
                color: Colors.blueAccent,
                size: 22.0,
              ),
            ),
            Container(
                child: Text(
              "Books",
              style: TextStyle(
                color: Colors.black54,
                fontSize: 22.0,
              ),
            )),
            SizedBox(width: 30.0),
          ],
        )),
        SizedBox(height: 5.0),
        Container(
            child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            SizedBox(width: 30.0),
            Container(
                child: Text(
              '${mapas.botella}',
              style: TextStyle(
                color: Colors.black54,
                fontSize: 22.0,
              ),
            )),
            Container(
              child: Icon(
                FontAwesomeIcons.wineBottle,
                color: Colors.blueAccent,
                size: 22.0,
              ),
            ),
            Container(
                child: Text(
              "Bottles",
              style: TextStyle(
                color: Colors.black54,
                fontSize: 22.0,
              ),
            )),
            SizedBox(width: 30.0),
          ],
        )),
        SizedBox(height: 10.0),
        Container(
            child: Text(
          "${mapas.ubicacion} ",
          style: TextStyle(
              color: Colors.black54,
              fontSize: 18.0,
              fontWeight: FontWeight.bold),
        )),
        SizedBox(height: 10.0),
        Container(
          width: 190,
          //   child: RaisedButton(
          //     color: Colors.blue,
          //     child: Row(children: <Widget>[
          //       Text(
          //         "Vacunarme",
          //         style: TextStyle(
          //             color: Colors.white,
          //             fontSize: 25.0,
          //             fontWeight: FontWeight.bold),
          //       ),
          //       Icon(
          //         Icons.double_arrow_outlined,
          //         color: Colors.white,
          //         size: 30.0,
          //       ),
          //     ]),
          //     shape: StadiumBorder(),
          //     onPressed: () {
          //       Navigator.pushReplacementNamed(context, 'calendar');
          //     },
          //   ),
          child: ElevatedButton(
              child: Row(children: <Widget>[
                Text(
                  "A Reciclar",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 26.0,
                      fontWeight: FontWeight.bold),
                ),
                Icon(
                  Icons.double_arrow_outlined,
                  color: Colors.white,
                  size: 28.0,
                ),
              ]),
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Colors.blue),
                shape: MaterialStateProperty.all<OutlinedBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(30.0),
                    ),
                  ),
                ),
              ),
              /*color: Colors.blue,
            textColor: Colors.white, //Se cambio para flutter 2.0 Y borrar los decaprecated
            shape: StadiumBorder(),*/
              onPressed: () => {
                    Navigator.pushReplacementNamed(context, 'punto',
                        arguments: mapas)
                  }),
        ),
      ],
    );
  }

  Widget _buildGoogleMap(BuildContext context, Set<Marker> markers) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: GoogleMap(
        // mapType: MapType.normal,
        mapType: tipomapa,
        initialCameraPosition:
            CameraPosition(target: LatLng(-11.0463731, -77.042754), zoom: 6),
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },

        markers: markers,
      ),
    );
  }

  Future<void> _gotoLocation(double lat, double long) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
      target: LatLng(lat, long),
      zoom: 15,
      tilt: 50.0,
      bearing: 45.0,
    )));
  }
}
