import 'package:flutter/material.dart';
import 'package:fluttericon/maki_icons.dart';
import 'package:spaceapp/src/models/mapas_model.dart';
import 'package:spaceapp/src/providers/mapa_provider.dart';
import 'package:spaceapp/src/widgets/menu_widget.dart';
// import 'package:url_launcher/url_launcher.dart';
import 'package:provider/provider.dart';
import 'package:spaceapp/src/theme/theme.dart';

class PuntoIdPage extends StatefulWidget {
  @override
  PuntoIdPageState createState() => PuntoIdPageState();
}

class PuntoIdPageState extends State<PuntoIdPage> {
  final puntosProvider = new MapaProvider();
  var color1;
  var color2;
  MapaDataModel punto = new MapaDataModel();

  final estiloTitulo = TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold);
  final estiloSubTitulo = TextStyle(fontSize: 18.0, color: Colors.grey);

  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;
    final MapaDataModel progData = ModalRoute.of(context).settings.arguments;
    if (progData != null) {
      punto = progData;
    }

    final appTheme2 = Provider.of<ThemeChanger>(context).currentTheme;

    return Scaffold(
        appBar: AppBar(
          title: Text(
            '${punto.nombre}',
            overflow: TextOverflow.ellipsis,
          ),
          centerTitle: true,
          // leading: IconButton(
          //   iconSize: 25,
          //   icon: const Icon(Icons.arrow_back_rounded),
          //   onPressed: () {
          //     Navigator.of(context).pop();
          //   },
          // ),
        ),
        drawer: MenuWidget(),
        body: _screenSize.width <= 720
            ? SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: _screenSize.width,
                      height: 300,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(30)),
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: NetworkImage(punto.imagen),
                          )),
                      // child: FadeInImage(
                      //   // height: 250,
                      //   placeholder: AssetImage('assets/giphy.gif'),
                      //   image: NetworkImage(punto.imagen),
                      //   // width: 100,
                      //   fadeInDuration: Duration(milliseconds: 200),
                      //   fit: BoxFit.cover,
                      // ),
                    ),
                    _crearTitulo(context, 0),

                    Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          _iconoDatos(
                              punto.botella, 'assets/icons/plastic.png', 26),
                          _iconoDatos(
                              punto.cuaderno, 'assets/icons/book.png', 26),
                          _iconoDatos(punto.bolsa, 'assets/icons/bag.png', 26),
                          _iconoDatos(punto.carton, 'assets/icons/box.png', 26),
                          // _iconoDatos('assets/icons/mask.png'),
                        ]),
                    SizedBox(height: 20),
                    // Icon(FontAwesome5.head_side_mask),
                    // _crearTexto(),
                    // SizedBox(height: 5),
                    // _crearPM(),
                    if (punto.botella >= 50)
                      _cardaviso('assets/cardmorado.png'),
                    if (punto.botella >= 30 && punto.botella < 50)
                      _cardaviso('assets/cardazul.png'),
                    if ((punto.botella < 30))
                      _cardaviso('assets/cardgreenlight.png'),
                    SizedBox(height: 5),
                    _crearBotones(),
                    SizedBox(height: 10),
                  ],
                ),
              )
            : Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <
                Widget>[
                Column(
                  children: <Widget>[
                    SizedBox(height: 20),
                    _crearTitulo(context, 1),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          _iconoDatos(
                              punto.botella, 'assets/icons/plastic.png', 60),
                          _iconoDatos(
                              punto.cuaderno, 'assets/icons/book.png', 60),
                          _iconoDatos(punto.bolsa, 'assets/icons/bag.png', 60),
                          _iconoDatos(punto.carton, 'assets/icons/box.png', 60),
                        ]),
                    SizedBox(height: 90),
                    if (punto.botella >= 50)
                      _cardaviso('assets/cardmorado.png'),
                    if (punto.botella >= 30 && punto.botella < 50)
                      _cardaviso('assets/cardazul.png'),
                    if ((punto.botella < 30))
                      _cardaviso('assets/cardgreenlight.png'),
                    SizedBox(height: 90),
                    _crearBotones(),
                  ],
                ),
                Container(
                  width: _screenSize.width / 2,
                  height: 1080,
                  decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.only(bottomLeft: Radius.circular(30)),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: NetworkImage(punto.imagen),
                      )),
                ),
              ]));
  }

  Widget _iconoDatos(int valor, String asseticon, double tamano) {
    return Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
      CircleAvatar(
        radius: tamano,
        backgroundColor: Colors.blue[50],
        child: Image(
          height: tamano,
          image: AssetImage(asseticon),
        ),
      ),
      Text(' : $valor', style: estiloSubTitulo),
      SizedBox(width: tamano / 2.6),
    ]);
  }

  Widget _cardaviso(String valor) {
    return Stack(
      children: <Widget>[
        Image(
          height: 150,
          image: AssetImage(valor),
          // width: 100,
          fit: BoxFit.cover,
        ),
        Positioned(
            right: 48,
            top: 8,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Message',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 20),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'Let\'s Recycle and ',
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 8,
                ),
                Text(
                  'support with',
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 8),
                Text(
                  'marine life',
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                )
              ],
            ))
      ],
    );
  }

  Widget _crearTitulo(BuildContext context, int valor) {
    final _screenSize = MediaQuery.of(context).size;
    final appTheme = Provider.of<ThemeChanger>(context);
    final accentColor = appTheme.currentTheme.primaryColor;
    return SafeArea(
      child: valor == 1
          ? Container(
              height: 100,
              width: _screenSize.width / 2,
              padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 20.0),
              child: _iconospuntos(accentColor),
            )
          : Container(
              padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 20.0),
              child: _iconospuntos(accentColor),
            ),
    );
  }

  Widget _iconospuntos(Color accentColor) {
    var starttamano = 20.0;
    var startcolor = Colors.purple;
    var startcolor2 = Colors.blue;
    var startcolor3 = Colors.green;
    return Center(
      child: Row(
        children: <Widget>[
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  '${punto.ubicacion}',
                  style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                      color: accentColor),
                  overflow: TextOverflow.ellipsis,
                ),
                SizedBox(width: 10.0),
                Row(
                  children: <Widget>[
                    if (punto.botella >= 50)
                      Row(children: <Widget>[
                        Icon(
                          Icons.circle,
                          color: startcolor,
                          size: starttamano,
                        ),
                        Icon(
                          Icons.circle,
                          color: startcolor,
                          size: starttamano,
                        ),
                        Icon(
                          Icons.circle,
                          color: startcolor,
                          size: starttamano,
                        ),
                        Icon(
                          Icons.circle,
                          color: startcolor,
                          size: starttamano,
                        ),
                        Icon(
                          Icons.circle,
                          color: startcolor,
                          size: starttamano,
                        ),
                      ]),
                    if (punto.botella >= 30 && punto.botella < 50)
                      Row(
                        children: [
                          Icon(
                            Icons.circle,
                            color: startcolor2,
                            size: starttamano,
                          ),
                          Icon(
                            Icons.circle,
                            color: startcolor2,
                            size: starttamano,
                          ),
                          Icon(
                            Icons.circle,
                            color: startcolor2,
                            size: starttamano,
                          ),
                          Icon(
                            Icons.circle,
                            color: startcolor2,
                            size: starttamano,
                          ),
                          Icon(
                            Icons.circle_outlined,
                            color: startcolor2,
                            size: starttamano,
                          ),
                        ],
                      ),
                    if (punto.botella < 30)
                      Row(
                        children: [
                          Icon(
                            Icons.circle,
                            color: startcolor3,
                            size: starttamano,
                          ),
                          Icon(
                            Icons.circle,
                            color: startcolor3,
                            size: starttamano,
                          ),
                          Icon(
                            Icons.circle,
                            color: startcolor3,
                            size: starttamano,
                          ),
                          Icon(
                            Icons.circle_outlined,
                            color: startcolor3,
                            size: starttamano,
                          ),
                          Icon(
                            Icons.circle_outlined,
                            color: startcolor3,
                            size: starttamano,
                          ),
                        ],
                      ),
                    SizedBox(width: 2.0),
                  ],
                )
              ],
            ),
          ),
          // CircleAvatar(
          //   radius: 28,
          //   backgroundColor: Color(0xff01a745),
          //   backgroundImage: AssetImage('assets/logointro.png'),
          // ),
        ],
      ),
    );
  }

  Widget _crearTexto() {
    final _screenSize = MediaQuery.of(context).size;
    return SafeArea(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 30.0),
        child: Text(
          '${punto.fecha}',
          textAlign: TextAlign.justify,
          overflow: TextOverflow.ellipsis,
          maxLines: 10,

          //overflow: TextOverflow.ellipsis,
        ),
      ),
    );
  }

  Widget _crearBotones() {
    return SafeArea(
      child: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                color: Colors.blue,
                child: punto.estatico == 1
                    ? Row(
                        children: <Widget>[
                          Icon(
                            Icons.web,
                            color: Colors.white,
                          ),
                          SizedBox(width: 10),
                          Text('See place',
                              style:
                                  TextStyle(fontSize: 18, color: Colors.white)),
                        ],
                      )
                    : Row(
                        children: <Widget>[
                          Icon(
                            Icons.camera,
                            color: Colors.white,
                          ),
                          SizedBox(width: 10),
                          Text('See live',
                              style:
                                  TextStyle(fontSize: 18, color: Colors.white)),
                        ],
                      ),
                onPressed: () {
                  Navigator.pushNamed(context, 'CamGalley', arguments: punto);

                  // _launchURL();
                }),
          ],
        ),
      ),
    );
  }

//   _launchURLWSP() async {
//     String cel = '${punto.celular}';
//     String url = 'https://api.whatsapp.com/send?phone=+51%20';
//     String urlwsp = '$url' '$cel';

//     if (await canLaunch(urlwsp)) {
//       await launch(urlwsp);
//     } else {
//       throw 'Could not launch $url';
//     }
//   }

//   _launchURL() async {
//     if (await canLaunch(punto.web)) {
//       await launch(punto.web);
//     } else {
//       throw 'Could not launch $punto.web';
//     }
//   }
}
