import 'package:spaceapp/src/models/mapas_model.dart';
import 'package:spaceapp/src/widgets/menu_widget.dart';
import 'package:flutter/material.dart';

class NascaPage extends StatelessWidget {
  MapaDataModel mapa =
      new MapaDataModel(); //Declarar modelo que pondremos los datos obtenidos

  @override
  Widget build(BuildContext context) {
    //Obtener datos de otra vista
    final MapaDataModel mapData = ModalRoute.of(context).settings.arguments;
    if (mapData != null) {
      mapa = mapData;
      print('Datos--> ${mapa.nombre}-$mapa');
    }

    //Obtener datos de otra vista
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Jorge Vicuña",
            textAlign: TextAlign.center,
          ),
        ),
        drawer: MenuWidget(),
        body: SingleChildScrollView(
          //  clipBehavior: Clip.antiAlias,

          child: Column(
            children: <Widget>[
              FadeInImage(
                placeholder: AssetImage('assets/jar-loading.gif'),
                image: NetworkImage(
                    "https://static.platzi.com/media/avatars/avatars/jorge150896_de70da0d-7a8d-4f54-93c2-4e0fed914d45.jpg"),
                fadeInDuration: Duration(milliseconds: 200),
                height: 200.0,
                fit: BoxFit.cover,
              ),
              // Image(
              //   image: NetworkImage('https://photographylife.com/wp-content/uploads/2017/01/What-is-landscape-photography.jpg'),
              // ),
              Center(
                child: Card(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      ListTile(
                        leading: Icon(
                          Icons.credit_card_rounded,
                          color: Colors.blue,
                        ),
                        title: Text.rich(
                          TextSpan(
                            // default text style
                            children: <TextSpan>[
                              TextSpan(
                                  text: 'DNI: ',
                                  style:
                                      TextStyle(fontWeight: FontWeight.bold)),
                              TextSpan(
                                  text: '70477575',
                                  style:
                                      TextStyle(fontStyle: FontStyle.italic)),
                            ],
                          ),
                        ),
                      ),
                      ListTile(
                        leading: Icon(
                          Icons.add_location_alt_rounded,
                          color: Colors.blue,
                        ),
                        title: Text.rich(
                          TextSpan(
                            // default text style
                            children: <TextSpan>[
                              TextSpan(
                                  text: 'Direccion: ',
                                  style:
                                      TextStyle(fontWeight: FontWeight.bold)),
                              TextSpan(
                                  text: 'Jr. 7 de Junio-Comas',
                                  style:
                                      TextStyle(fontStyle: FontStyle.italic)),
                            ],
                          ),
                        ),
                      ),
                      ListTile(
                        leading: Icon(
                          Icons.bento_outlined,
                          color: Colors.blue,
                        ),
                        title: Text.rich(
                          TextSpan(
                            // default text style
                            children: <TextSpan>[
                              TextSpan(
                                  text: 'Edad: ',
                                  style:
                                      TextStyle(fontWeight: FontWeight.bold)),
                              TextSpan(
                                  text: '24',
                                  style:
                                      TextStyle(fontStyle: FontStyle.italic)),
                            ],
                          ),
                        ),
                      ),
                      ListTile(
                        leading: Icon(
                          Icons.healing_sharp,
                          color: Colors.blue,
                        ),
                        title: Text.rich(
                          TextSpan(
                            // default text style
                            children: <TextSpan>[
                              TextSpan(
                                  text: 'Tiempo de vacunacion: ',
                                  style:
                                      TextStyle(fontWeight: FontWeight.bold)),
                              TextSpan(
                                  text: '2 semanas',
                                  style:
                                      TextStyle(fontStyle: FontStyle.italic)),
                            ],
                          ),
                        ),
                      ),
                      ListTile(
                        leading: Icon(
                          Icons.medical_services_outlined,
                          color: Colors.blue,
                        ),
                        title: Text.rich(
                          TextSpan(
                            // default text style
                            children: <TextSpan>[
                              TextSpan(
                                  text: 'Alergia: ',
                                  style:
                                      TextStyle(fontWeight: FontWeight.bold)),
                              TextSpan(
                                  text: 'Alergia1 ,',
                                  style:
                                      TextStyle(fontStyle: FontStyle.italic)),
                              TextSpan(
                                  text: 'Alergia2 ,',
                                  style:
                                      TextStyle(fontStyle: FontStyle.italic)),
                              TextSpan(
                                  text: 'Alergia3',
                                  style:
                                      TextStyle(fontStyle: FontStyle.italic)),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              FadeInImage(
                placeholder: AssetImage('assets/jar-loading.gif'),
                image: AssetImage('assets/scan.png'),
                fadeInDuration: Duration(milliseconds: 200),
                height: 200.0,
                fit: BoxFit.cover,
              ),
            ],
          ),
        ));
  }
}
