import 'dart:async';

import 'package:flutter/material.dart';
import 'package:animate_do/animate_do.dart';

class SplashScreenPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => StartState();
}

class StartState extends State<SplashScreenPage> {
  @override
  Widget build(BuildContext context) {
    return initScreen(context);
  }

  @override
  void initState() {
    super.initState();
    startTimer();
  }

  startTimer() async {
    var duration = Duration(seconds: 5);
    return new Timer(duration, route);
  }

  route() {
    // Navigator.pushReplacement(
    //     context, MaterialPageRoute(builder: (context) => MapaPage()));
    Navigator.pushReplacementNamed(context, 'Mapa');
  }

  initScreen(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            FadeInDown(
              delay: Duration(
                milliseconds: 1000,
              ),
              child: Container(
                height: 250,
                child: Image.asset("assets/spaceappintro.png"),
              ),
            ),
            Padding(padding: EdgeInsets.only(top: 20.0)),
            Text(
              "Cargando...",
              style: TextStyle(fontSize: 20.0, color: Color(0xff00578b)),
            ),
            Padding(padding: EdgeInsets.only(top: 20.0)),
            CircularProgressIndicator(
              backgroundColor: Colors.blueAccent,
              strokeWidth: 1,
            )
          ],
        ),
      ),
    );
  }
}
