import 'package:flutter/material.dart';
import 'package:spaceapp/src/widgets/menu_widget.dart';

import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:fwfh_webview/fwfh_webview.dart';

class CanvaPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _screeSize = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: Text('About'),
      ),
      drawer: MenuWidget(),
      body: Center(
          child: _screeSize.width >= 600
              ? Container(
                  color: Colors.black,
                  width: _screeSize.width / 1.5,
                  height: double.infinity,
                  child: HtmlWidget(
                    '<iframe src="https:&#x2F;&#x2F;www.canva.com&#x2F;design&#x2F;DAErnjz3WTM&#x2F;view?embed"></iframe>',
                    factoryBuilder: () => MyWidgetFactory(),
                  ),
                )
              : Container(
                  color: Colors.black,
                  width: _screeSize.width,
                  height: double.infinity,
                  child: HtmlWidget(
                    '<iframe src=https:&#x2F;&#x2F;www.canva.com&#x2F;design&#x2F;DAErnjz3WTM&#x2F;view?embed"></iframe>',
                    factoryBuilder: () => MyWidgetFactory(),
                  ),
                )),
    );
  }
}

class MyWidgetFactory extends WidgetFactory with WebViewFactory {
  // optional: override getter to configure how WebViews are built
  bool get webViewMediaPlaybackAlwaysAllow => true;
  String get webViewUserAgent => 'My app';
}
