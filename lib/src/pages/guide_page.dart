import 'package:flutter/material.dart';
import 'package:spaceapp/src/widgets/menu_widget.dart';

import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:fwfh_webview/fwfh_webview.dart';

class GuidePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _screeSize = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: Text('About'),
      ),
      drawer: MenuWidget(),
      body: Center(
          child: _screeSize.width >= 600
              ? Container(
                  width: _screeSize.width / 1.5,
                  height: double.infinity,
                  child: HtmlWidget(
                    '<iframe src="https://2021.spaceappschallenge.org/challenges/statements/leveraging-aiml-for-plastic-marine-debris/teams/qhipa-pacha-code/project"></iframe>',
                    factoryBuilder: () => MyWidgetFactory(),
                  ),
                )
              : Container(
                  width: _screeSize.width,
                  height: double.infinity,
                  child: HtmlWidget(
                    '<iframe src="https://2021.spaceappschallenge.org/challenges/statements/leveraging-aiml-for-plastic-marine-debris/teams/qhipa-pacha-code/project"></iframe>',
                    factoryBuilder: () => MyWidgetFactory(),
                  ),
                )),
    );
  }
}

class MyWidgetFactory extends WidgetFactory with WebViewFactory {
  // optional: override getter to configure how WebViews are built
  bool get webViewMediaPlaybackAlwaysAllow => true;
  String get webViewUserAgent => 'My app';
}
