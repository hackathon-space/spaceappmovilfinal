import 'package:flutter/material.dart';
//import 'package:spaceapp/src/bloc/provider.dart';

import 'package:spaceapp/src/models/mapas_model.dart';
import 'package:spaceapp/src/providers/mapa_provider.dart';

import 'package:spaceapp/src/theme/theme.dart';
import 'package:spaceapp/src/widgets/menu_widget.dart';
import 'package:provider/provider.dart';

class PuntosReciclajePage extends StatefulWidget {
  // lo convertir en Statefull, porque noa ctualizaba las imagenes que subia

  @override
  _ProgramasPageState createState() => _ProgramasPageState();
}

class _ProgramasPageState extends State<PuntosReciclajePage> {
  final mapasProvider = new MapaProvider();
  var color1;

  @override
  Widget build(BuildContext context) {
    //final bloc = Provider.of(context);
    final appTheme2 = Provider.of<ThemeChanger>(context).currentTheme;
    if (appTheme2.scaffoldBackgroundColor == Color(0xff16202B)) {
      color1 = Colors.white24;
    } else {
      color1 = Colors.black26;
    }

    return Scaffold(
      appBar: AppBar(title: Text('Mapped beaches')),
      drawer: MenuWidget(),
      body: _crearListado(),
    );
  }

  Widget _crearListado() {
    final ScrollController scrollController = ScrollController();
    final _screeSize = MediaQuery.of(context).size;
    return FutureBuilder(
      future: mapasProvider.cargarMapa(),
      builder:
          (BuildContext context, AsyncSnapshot<List<MapaDataModel>> snapshot) {
        if (snapshot.hasData) {
          final puntos = snapshot.data;

          return SizedBox(
              // width: tamano,
              child: Scrollbar(
                  isAlwaysShown: true,
                  controller: scrollController,
                  child: _screeSize.width <= 720
                      ? ListView.builder(
                          controller: scrollController,
                          itemCount: puntos.length,
                          itemBuilder: (context, i) =>
                              _crearItem(context, puntos[i]),
                        )
                      : _screeSize.width >= 720 && _screeSize.width <= 1200
                          ? GridView.builder(
                              controller: scrollController,
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 2),
                              itemCount: puntos.length,
                              itemBuilder: (context, i) {
                                return Container(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: _crearItem(context, puntos[i]),
                                );
                              })
                          : GridView.builder(
                              controller: scrollController,
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 4),
                              itemCount: puntos.length,
                              itemBuilder: (context, i) {
                                return Container(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: _crearItem(context, puntos[i]),
                                );
                              })));
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  Widget _crearItem(BuildContext context, MapaDataModel mapa) {
    return Container(
      child: Column(
        children: <Widget>[
          (mapa.imagen == null)
              ? Image(image: AssetImage('assets/no-image.png'))
              : _cardTipo2(mapa),
        ],
      ),
    );
  }

  Widget _cardTipo2(MapaDataModel mapa) {
    final _screenSize = MediaQuery.of(context).size;

    final card = Container(
      child: Column(
        children: <Widget>[
          FadeInImage(
            image: NetworkImage(mapa.imagen),
            placeholder: AssetImage('assets/jar-loading.gif'),
            fadeInDuration: Duration(milliseconds: 200),
            height: 250,
            width: double.infinity,
            fit: BoxFit.cover,
          ),
          ListTile(
            title: Text('${mapa.nombre}',
                overflow: TextOverflow.ellipsis,
                style: TextStyle(color: Colors.black)),
            subtitle: Container(
                child: Row(
              children: <Widget>[
                if (mapa.botella >= 50)
                  Icon(
                    Icons.circle,
                    color: Colors.purple,
                    size: 10.0,
                  ),
                if (mapa.botella >= 30 && mapa.botella < 50)
                  Icon(
                    Icons.circle,
                    color: Colors.blue,
                    size: 10.0,
                  ),
                if (mapa.botella < 30)
                  Icon(
                    Icons.circle,
                    color: Colors.green,
                    size: 10.0,
                  ),
                SizedBox(width: 2.0),
                if (mapa.botella >= 50)
                  Text("Let's recycle",
                      style: TextStyle(color: Colors.black54)),
                if (mapa.botella >= 30 && mapa.botella < 50)
                  Text("Let's recycle and help",
                      style: TextStyle(color: Colors.black54)),
                if (mapa.botella < 30)
                  Text("We are going to clean",
                      style: TextStyle(color: Colors.black54)),
              ],
            )),
            //onTap: () => Navigator.pushNamed(context, 'producto', arguments: producto ), // lo cambie para actualizar home
            onTap: () => Navigator.pushNamed(context, 'punto', arguments: mapa)
                .then((value) {
              setState(() {});
            }),
          ),
        ],
      ),
    );
    return Column(
      children: <Widget>[
        SizedBox(height: 18.0),
        Container(
          width: _screenSize.width - 25,
          // padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30.0),
              color: Colors.white,
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: color1,
                    blurRadius: 10.0,
                    spreadRadius: 2.0,
                    offset: Offset(2.0, 10.0))
              ]),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(30.0),
            child: card,
          ),
        ),
        SizedBox(height: 8),
      ],
    );
  }
}
