import 'package:flutter/material.dart';
import 'package:spaceapp/src/widgets/menu_widget.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:http/http.dart' as http;

class GuidePagenormal extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('About')),
      drawer: MenuWidget(),
      body: FutureBuilder(
          future: http.get(Uri.parse(
              'https://2021.spaceappschallenge.org/challenges/statements/leveraging-aiml-for-plastic-marine-debris/teams/qhipa-pacha-code/project')),
          builder: (BuildContext context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(child: CircularProgressIndicator());
            } else {
              return Container(
                child: WebView(
                  initialUrl:
                      "https://2021.spaceappschallenge.org/challenges/statements/leveraging-aiml-for-plastic-marine-debris/teams/qhipa-pacha-code/project",
                  javascriptMode: JavascriptMode.unrestricted,
                ),
              );
            }
          }),
    );
  }
}
