import 'package:flutter/material.dart';
import 'package:spaceapp/src/models/mapas_model.dart';
import 'package:spaceapp/src/providers/mapa_provider.dart';
import 'package:spaceapp/src/widgets/menu_widget.dart';
// import 'package:url_launcher/url_launcher.dart';
import 'package:provider/provider.dart';
import 'package:spaceapp/src/theme/theme.dart';
// import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

import 'package:spaceapp/src/theme/theme.dart';
import 'package:provider/provider.dart';

class UserPage extends StatefulWidget {
  @override
  EnivoPageState createState() => EnivoPageState();
}

class EnivoPageState extends State<UserPage> {
  final puntosProvider = new MapaProvider();
  var color1;
  var color2;

  MapaDataModel punto = new MapaDataModel();

  final estiloTitulo = TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold);
  final estiloSubTitulo = TextStyle(fontSize: 18.0, color: Colors.grey);

  YoutubePlayerController _controller = YoutubePlayerController(
      initialVideoId: 'fY0DP2o1Pzg', // id youtube video
      params: const YoutubePlayerParams(
        playlist: ['fY0DP2o1Pzg', 'S3gysPJETGA', 'mAOEE3aRYDM'],
        showControls: true,
        // showFullscreenButton: true,
        desktopMode: true,
        autoPlay: true,
      ));

  @override
  Widget build(BuildContext context) {
    final appTheme = Provider.of<ThemeChanger>(context);
    final accentColor = appTheme.currentTheme.primaryColor;
    final _screenSize = MediaQuery.of(context).size;
    final MapaDataModel progData = ModalRoute.of(context).settings.arguments;
    if (progData != null) {
      punto = progData;
    }
    final appTheme2 = Provider.of<ThemeChanger>(context).currentTheme;

    if (appTheme2.scaffoldBackgroundColor == Color(0xff16202B)) {
      color1 = Colors.white24;
    } else {
      color1 = Colors.black26;
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'User Guide',
          overflow: TextOverflow.ellipsis,
        ),
        centerTitle: true,
      ),
      drawer: MenuWidget(),
      body: _screenSize.width <= 720
          ? SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  // Icon(FontAwesome5.head_side_mask),
                  // _crearTexto(),
                  // SizedBox(height: 5),
                  // _crearPM(),

                  Container(
                    height: 250,
                    child: YoutubePlayerControllerProvider(
                      // Provides controller to all the widget below it.
                      controller: _controller,
                      child: YoutubePlayerIFrame(
                        aspectRatio: 16 / 9,
                      ),
                    ),
                  ),
                  SizedBox(height: 25),
                  Text('PLAYLIST'),
                  Container(
                      padding: EdgeInsets.all(15),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text('- Presentation of the project'),
                            Text('- Detection of contaminated objects'),
                            Text('- App Móvil Recicl-IA'),
                          ])),
                  SizedBox(height: 25),
                  Text('DESCRIPTION'),
                  Container(
                    padding: EdgeInsets.all(15),
                    child: Text(
                        'Our project stands out in the use of Artificial Intelligence by computer vision for the collection of specific data, which implies in that sense analyzing the pollution seen in the sea (bottles, boxes, bags, masks) for programming we rely on the Python language and Open CV. This is possible thanks to the placement of smart poles for data collection and analysis, to extend our purpose of recycling and points of contamination we will use drones with the same device. Likewise, we will proceed to collect them at geolocated points to locate ourselves on a map, we will also integrate an app developed in Flutter for recyclers, where they can see the closest collection points. This will detect some hazardous wastes in the oceanic area which are inaccessible to citizens, therefore this information will be displayed in a Dashboard to the relevant authorities for collection and cleaning.The impact that our project will have is to be able to contribute and actively promote the culture of recycling on the busiest beaches in the country and in the world, likewise; Pollution can be reduced and therefore marine life improved.'),
                  ),
                  _cardTipo2('https://i.imgur.com/msjLr8O.png',
                      'Detection of contaminated objects', "Presentation 1"),
                  _cardTipo2('https://i.imgur.com/rlFyPab.png',
                      'Projetc System', "Presentation 2"),
                  SizedBox(height: 25),
                ],
              ),
            )
          : SingleChildScrollView(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      // Icon(FontAwesome5.head_side_mask),
                      // _crearTexto(),
                      // SizedBox(height: 5),
                      // _crearPM(),
                      SizedBox(width: _screenSize.width / 36),
                      Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            SizedBox(height: 25),
                            Text(
                              'PLAYLIST',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            Container(
                                padding: EdgeInsets.all(15),
                                child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text('- Presentation of the project'),
                                      Text(
                                          '- Detection of contaminated objects'),
                                      Text('- App Móvil Recicl-IA'),
                                    ])),
                            SizedBox(height: 25),
                            Text(
                              'DESCRIPTION',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            Container(
                              width: _screenSize.width / 2.5,
                              padding: EdgeInsets.all(15),
                              child: Text(
                                  'Our project stands out in the use of Artificial Intelligence by computer vision for the collection of specific data, which implies in that sense analyzing the pollution seen in the sea (bottles, boxes, bags, masks) for programming we rely on the Python language and Open CV. This is possible thanks to the placement of smart poles for data collection and analysis, to extend our purpose of recycling and points of contamination we will use drones with the same device. Likewise, we will proceed to collect them at geolocated points to locate ourselves on a map, we will also integrate an app developed in Flutter for recyclers, where they can see the closest collection points. This will detect some hazardous wastes in the oceanic area which are inaccessible to citizens, therefore this information will be displayed in a Dashboard to the relevant authorities for collection and cleaning.The impact that our project will have is to be able to contribute and actively promote the culture of recycling on the busiest beaches in the country and in the world, likewise; Pollution can be reduced and therefore marine life improved.'),
                            ),
                          ]),
                      SizedBox(height: 25),
                      Container(
                        height: 600,
                        width: _screenSize.width / 1.9,
                        child: YoutubePlayerControllerProvider(
                          // Provides controller to all the widget below it.
                          controller: _controller,
                          child: YoutubePlayerIFrame(
                            aspectRatio: 16 / 9,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        _cardTipo3(
                            'https://i.imgur.com/msjLr8O.png',
                            'Detection of contaminated objects',
                            "Presentation 1"),
                        SizedBox(width: 25),
                        _cardTipo3('https://i.imgur.com/rlFyPab.png',
                            'Projetc System', "Presentation 2"),
                      ]),
                ])),
    );
  }

  Widget _cardTipo2(String urll, String Titulo, String description) {
    final _screenSize = MediaQuery.of(context).size;

    final card = Container(
      child: Column(
        children: <Widget>[
          FadeInImage(
            image: NetworkImage(urll),
            placeholder: AssetImage('assets/jar-loading.gif'),
            fadeInDuration: Duration(milliseconds: 200),
            // height: 220,
            width: double.infinity,
            fit: BoxFit.cover,
          ),
          ListTile(
              title: Text(Titulo,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(color: Colors.black)),
              subtitle: Container(
                  child: Row(
                children: <Widget>[
                  Text(description, style: TextStyle(color: Colors.black54)),
                ],
              )),
              //onTap: () => Navigator.pushNamed(context, 'producto', arguments: producto ), // lo cambie para actualizar home
              onTap: () => {}),
        ],
      ),
    );
    return Column(
      children: <Widget>[
        SizedBox(height: 18.0),
        Container(
          width: _screenSize.width - 25,
          // padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30.0),
              color: Colors.white,
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: color1,
                    blurRadius: 10.0,
                    spreadRadius: 2.0,
                    offset: Offset(2.0, 10.0))
              ]),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(30.0),
            child: card,
          ),
        ),
        SizedBox(height: 8),
      ],
    );
  }

  Widget _cardTipo3(String urll, String Titulo, String description) {
    final _screenSize = MediaQuery.of(context).size;

    final card = Container(
      child: Column(
        children: <Widget>[
          FadeInImage(
            image: NetworkImage(urll),
            placeholder: AssetImage('assets/jar-loading.gif'),
            fadeInDuration: Duration(milliseconds: 200),
            // height: 220,
            width: double.infinity,
            fit: BoxFit.cover,
          ),
          ListTile(
              title: Text(Titulo,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(color: Colors.black)),
              subtitle: Container(
                  child: Row(
                children: <Widget>[
                  Text(description, style: TextStyle(color: Colors.black54)),
                ],
              )),
              //onTap: () => Navigator.pushNamed(context, 'producto', arguments: producto ), // lo cambie para actualizar home
              onTap: () => {}),
        ],
      ),
    );
    return Column(
      children: <Widget>[
        SizedBox(height: 18.0),
        Container(
          width: _screenSize.width / 2 - 25,
          // padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30.0),
              color: Colors.white,
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: color1,
                    blurRadius: 10.0,
                    spreadRadius: 2.0,
                    offset: Offset(2.0, 10.0))
              ]),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(30.0),
            child: card,
          ),
        ),
        SizedBox(height: 8),
      ],
    );
  }
}
