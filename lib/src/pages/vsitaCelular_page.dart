import 'package:flutter/material.dart';
import 'package:spaceapp/src/widgets/menu_widget.dart';

import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:fwfh_webview/fwfh_webview.dart';

class MovilPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _screeSize = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: Text('Mobile View'),
      ),
      drawer: MenuWidget(),
      body: Center(
          child: _screeSize.width >= 700
              ? Container(
                  color: Colors.black,
                  width: _screeSize.width / 3.1,
                  height: double.infinity,
                  child: HtmlWidget(
                    '<iframe src="https://spaceapp-recyclia.netlify.app/"></iframe>',
                    factoryBuilder: () => MyWidgetFactory(),
                  ),
                )
              : Container(
                  width: _screeSize.width,
                  height: double.infinity,
                  child: HtmlWidget(
                    '<iframe src="https://spaceapp-recyclia.netlify.app/"></iframe>',
                    factoryBuilder: () => MyWidgetFactory(),
                  ),
                )),
    );
  }
}

class MyWidgetFactory extends WidgetFactory with WebViewFactory {
  // optional: override getter to configure how WebViews are built
  bool get webViewMediaPlaybackAlwaysAllow => true;
  String get webViewUserAgent => 'My app';
}
