import 'package:flutter/material.dart';
import 'package:spaceapp/src/models/mapas_model.dart';
import 'package:spaceapp/src/providers/mapa_provider.dart';
import 'package:spaceapp/src/widgets/menu_widget.dart';
// import 'package:url_launcher/url_launcher.dart';
import 'package:provider/provider.dart';
import 'package:spaceapp/src/theme/theme.dart';
// import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

import 'package:spaceapp/src/theme/theme.dart';
import 'package:provider/provider.dart';

class EnvivoPage extends StatefulWidget {
  @override
  EnivoPageState createState() => EnivoPageState();
}

class EnivoPageState extends State<EnvivoPage> {
  final puntosProvider = new MapaProvider();
  var color1;
  var color2;

  MapaDataModel punto = new MapaDataModel();

  final estiloTitulo = TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold);
  final estiloSubTitulo = TextStyle(fontSize: 18.0, color: Colors.grey);

  YoutubePlayerController _controller = YoutubePlayerController(
      initialVideoId: 'S3gysPJETGA', // id youtube video
      params: const YoutubePlayerParams(
        showControls: true,
        // showFullscreenButton: true,
        desktopMode: false,
        autoPlay: true,
      ));

  @override
  Widget build(BuildContext context) {
    final appTheme = Provider.of<ThemeChanger>(context);
    final accentColor = appTheme.currentTheme.primaryColor;
    final _screenSize = MediaQuery.of(context).size;
    final MapaDataModel progData = ModalRoute.of(context).settings.arguments;
    if (progData != null) {
      punto = progData;
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(
          '${punto.nombre}',
          overflow: TextOverflow.ellipsis,
        ),
        centerTitle: true,
        leading: IconButton(
          iconSize: 25,
          icon: const Icon(Icons.arrow_back_rounded),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      // drawer: MenuWidget(),
      body: _screenSize.width <= 720
          ? SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  // Icon(FontAwesome5.head_side_mask),
                  // _crearTexto(),
                  // SizedBox(height: 5),
                  // _crearPM(),

                  Container(
                    child: YoutubePlayerControllerProvider(
                      // Provides controller to all the widget below it.
                      controller: _controller,
                      child: YoutubePlayerIFrame(
                        aspectRatio: 16 / 9,
                      ),
                    ),
                  ),
                  SizedBox(height: 25),
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: <
                      Widget>[
                    _iconoDatos(punto.botella, 'assets/icons/plastic.png', 26),
                    _iconoDatos(punto.cuaderno, 'assets/icons/book.png', 26),
                    _iconoDatos(punto.bolsa, 'assets/icons/bag.png', 26),
                    _iconoDatos(punto.carton, 'assets/icons/box.png', 26),
                    // _iconoDatos('assets/icons/mask.png'),
                  ]),

                  SizedBox(height: 35),
                  _crearActoresPageView(200, 150, 120, accentColor),
                  _listaTarjeta(Colors.blue, 'Preguntas Frecuentes'),
                  _listaTarjeta(Colors.blue, 'Recomendaciones'),
                ],
              ),
            )
          : SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  // Icon(FontAwesome5.head_side_mask),
                  // _crearTexto(),
                  // SizedBox(height: 5),
                  // _crearPM(),

                  Container(
                    height: 500,
                    child: YoutubePlayerControllerProvider(
                      // Provides controller to all the widget below it.
                      controller: _controller,
                      child: YoutubePlayerIFrame(
                        aspectRatio: 16 / 9,
                      ),
                    ),
                  ),
                  SizedBox(height: 25),
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: <
                      Widget>[
                    _iconoDatos(punto.botella, 'assets/icons/plastic.png', 60),
                    _iconoDatos(punto.cuaderno, 'assets/icons/book.png', 60),
                    _iconoDatos(punto.bolsa, 'assets/icons/bag.png', 60),
                    _iconoDatos(punto.carton, 'assets/icons/box.png', 60),
                    // _iconoDatos('assets/icons/mask.png'),
                  ]),

                  SizedBox(height: 35),
                  _crearActoresPageView(320, 300, 250, accentColor),
                  _listaTarjeta(Colors.blue, 'Preguntas Frecuentes'),
                  _listaTarjeta(Colors.blue, 'Recomendaciones'),
                ],
              ),
            ),
    );
  }

  Widget _crearActoresPageView(
      double alto, double alto2, double ancho, Color accentColor) {
    return SizedBox(
      height: alto,
      child: PageView.builder(
        pageSnapping: false,
        controller: PageController(viewportFraction: 0.3, initialPage: 1),
        itemCount: 3,
        itemBuilder: (context, i) =>
            _hotelTarjeta(i, alto2, ancho, accentColor),
      ),
    );
  }

  Widget _hotelTarjeta(int i, double alto2, double ancho, Color accentColor) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 5.0),
        child: Column(
          children: <Widget>[
            ClipRRect(
                borderRadius: BorderRadius.circular(10.0),
                child: i == 0
                    ? FadeInImage(
                        image: NetworkImage(punto.imagencontaminada1),
                        placeholder: AssetImage('assets/giphy.gif'),
                        height: alto2,
                        width: ancho,
                        fit: BoxFit.cover,
                      )
                    : i == 1
                        ? FadeInImage(
                            image: NetworkImage(punto.imagencontaminada2),
                            placeholder: AssetImage('assets/giphy.gif'),
                            height: alto2,
                            width: ancho,
                            fit: BoxFit.cover,
                          )
                        : FadeInImage(
                            image: NetworkImage(punto.imagencontaminada3),
                            placeholder: AssetImage('assets/giphy.gif'),
                            height: alto2,
                            width: ancho,
                            fit: BoxFit.cover,
                          )),
            Text(
              'Captura',
              overflow: TextOverflow.ellipsis,
              style: TextStyle(color: accentColor),
            )
          ],
        ));
  }

  Widget _listaTarjeta(Color ValorColor, String ValorTexto) {
    return Container(
      padding: EdgeInsets.all(5),
      child: Column(children: <Widget>[
        ListTile(
          title: Text(ValorTexto, style: TextStyle(color: ValorColor)),
          leading: Icon(
            Icons.folder_open,
            color: ValorColor,
          ),
          trailing: Icon(Icons.keyboard_arrow_right, color: ValorColor),
          onTap: () {
            //Navigator.pushNamed(context, opt['ruta'] );
          },
        ),
      ]),
    );
  }

  Widget _iconoDatos(int valor, String asseticon, double tamano) {
    return Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
      CircleAvatar(
        radius: tamano,
        backgroundColor: Colors.blue[50],
        child: Image(
          height: tamano,
          image: AssetImage(asseticon),
        ),
      ),
      Text(' : $valor', style: estiloSubTitulo),
      SizedBox(width: tamano / 2.6),
    ]);
  }
}
