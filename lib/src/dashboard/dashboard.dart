// @dart=2.9
import 'package:flutter/material.dart';
import 'package:spaceapp/src/dashboard/analyricsPage.dart';
import 'package:spaceapp/src/widgets/menu_widget.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Scaffold(
        appBar: AppBar(title: Text('Dashboard')),
        drawer: MenuWidget(),
        backgroundColor: Colors.white,
        body: Container(
          child: Row(
            children: <Widget>[
              // Container(
              //   child: SideNavBar(),
              // ),
              Expanded(
                child: AnalyticsPage(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
