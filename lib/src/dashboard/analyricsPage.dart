import 'package:flutter/material.dart';
import 'package:fluttericon/entypo_icons.dart';
import 'package:spaceapp/src/dashboard/chart.dart';
import 'package:spaceapp/src/dashboard//lineChart.dart';

import 'package:fluttericon/font_awesome5_icons.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';
import 'package:spaceapp/src/providers/mapa_provider.dart';

class AnalyticsPage extends StatelessWidget {
  final dataDashboard = new MapaProvider();
  YoutubePlayerController _controller = YoutubePlayerController(
      initialVideoId: 'S3gysPJETGA', // id youtube video
      params: const YoutubePlayerParams(
        showControls: true,
        // showFullscreenButton: true,
        desktopMode: false,
        autoPlay: true,
      ));

  // Widget getDataDashboard() {
  //   return FutureBuilder(
  //     future: dataDashboard.DataDashboard(),

  //   );
  // }

  @override
  Widget build(BuildContext context) {
    var currentWidth = MediaQuery.of(context).size.width;
    var extraLargeScreenGrid = currentWidth > 1536;
    var largeScreenGrid = currentWidth > 1366;
    var smallScreenGrid = currentWidth > 1201;
    var tabScreenGrid = currentWidth > 769;
    // getDataDashboard();
    // dynamic dataDash =  dataDashboard.DataDashboard();
    // DashboardDataModel model = DashboardDataModel.fromJson(dataDashboard.DataDashboard());
    var dataDash = dataDashboard.DataDashboard();
    // print('llegue');
    // Map<String, dynamic> myMaps = dataDashboard.DataDashboard();
    // print(myMaps);
    return Container(
      alignment: Alignment.topLeft,
      color: Colors.black12.withOpacity(0.03),
      child: Stack(
        alignment: Alignment.topLeft,
        children: [
          SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Container(
                  margin: EdgeInsets.only(
                      left: 25.0, top: 50.0, right: 25.0, bottom: 12.5),
                  child: new GridView.count(
                    crossAxisCount: extraLargeScreenGrid
                        ? 4
                        : tabScreenGrid
                            ? 4
                            : 1,
                    childAspectRatio: 3.5,
                    mainAxisSpacing: 20.0,
                    crossAxisSpacing: 20.0,
                    padding: EdgeInsets.all(02.0),
                    shrinkWrap: true,
                    children: [
                      Container(
                        child: Material(
                          elevation: 02.0,
                          borderRadius: BorderRadius.circular(03.0),
                          child: Container(
                            color: Colors.blue[300],
                            height: 120.0,
                            padding: EdgeInsets.all(0.0),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Container(
                                            padding:
                                                EdgeInsets.only(left: 15.0),
                                            child: Text(
                                              'Bottles',
                                              style: TextStyle(
                                                fontSize: 15.0,
                                                fontWeight: FontWeight.w500,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Container(
                                            padding:
                                                EdgeInsets.only(left: 15.0),
                                            child: Text(
                                              '1802',
                                              style: TextStyle(
                                                fontSize: 40.0,
                                                fontWeight: FontWeight.w600,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  width: 120.0,
                                  height: 120.0,
                                  child: Icon(
                                    FontAwesome5.wine_bottle,
                                    size: 50.0,
                                    color: Colors.white,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Container(
                        child: Material(
                          elevation: 02.0,
                          borderRadius: BorderRadius.circular(03.0),
                          child: Container(
                            color: Colors.red[300],
                            height: 120.0,
                            padding: EdgeInsets.all(0.0),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Container(
                                            padding:
                                                EdgeInsets.only(left: 15.0),
                                            child: Text(
                                              'Books',
                                              style: TextStyle(
                                                fontSize: 15.0,
                                                fontWeight: FontWeight.w500,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Container(
                                            padding:
                                                EdgeInsets.only(left: 15.0),
                                            child: Text(
                                              '1826',
                                              style: TextStyle(
                                                fontSize: 40.0,
                                                fontWeight: FontWeight.w600,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  width: 120.0,
                                  height: 120.0,
                                  child: Icon(
                                    FontAwesome5.book,
                                    size: 50.0,
                                    color: Colors.white,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Container(
                        child: Material(
                          elevation: 02.0,
                          borderRadius: BorderRadius.circular(03.0),
                          child: Container(
                            color: Colors.orange[300],
                            height: 120.0,
                            padding: EdgeInsets.all(0.0),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Container(
                                            padding:
                                                EdgeInsets.only(left: 15.0),
                                            child: Text(
                                              'Cardboard boxes',
                                              style: TextStyle(
                                                fontSize: 15.0,
                                                fontWeight: FontWeight.w500,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Container(
                                            padding:
                                                EdgeInsets.only(left: 15.0),
                                            child: Text(
                                              '142',
                                              style: TextStyle(
                                                fontSize: 40.0,
                                                fontWeight: FontWeight.w600,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  width: 120.0,
                                  height: 120.0,
                                  child: Icon(
                                    FontAwesome5.dropbox,
                                    size: 50.0,
                                    color: Colors.white,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Container(
                        child: Material(
                          elevation: 02.0,
                          borderRadius: BorderRadius.circular(03.0),
                          child: Container(
                            color: Colors.indigo[300],
                            height: 120.0,
                            padding: EdgeInsets.all(0.0),
                            child: DecoratedBox(
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: Colors.black12, width: 01.0),
                                borderRadius: BorderRadius.circular(03.0),
                              ),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Container(
                                              padding:
                                                  EdgeInsets.only(left: 15.0),
                                              child: Text(
                                                'Bags',
                                                style: TextStyle(
                                                  fontSize: 15.0,
                                                  fontWeight: FontWeight.w500,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Container(
                                              padding:
                                                  EdgeInsets.only(left: 15.0),
                                              child: Text(
                                                '1802',
                                                style: TextStyle(
                                                  fontSize: 40.0,
                                                  fontWeight: FontWeight.w600,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    width: 120.0,
                                    height: 120.0,
                                    child: Icon(
                                      Entypo.bag,
                                      size: 50.0,
                                      color: Colors.white,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        child: Material(
                          elevation: 02.0,
                          borderRadius: BorderRadius.circular(03.0),
                          child: Container(
                            color: Colors.indigo[300],
                            height: 120.0,
                            padding: EdgeInsets.all(0.0),
                            child: DecoratedBox(
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: Colors.black12, width: 01.0),
                                borderRadius: BorderRadius.circular(03.0),
                              ),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Container(
                                              padding:
                                                  EdgeInsets.only(left: 15.0),
                                              child: Text(
                                                'FACE MASK',
                                                style: TextStyle(
                                                  fontSize: 15.0,
                                                  fontWeight: FontWeight.w500,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Container(
                                              padding:
                                                  EdgeInsets.only(left: 15.0),
                                              child: Text(
                                                '415',
                                                style: TextStyle(
                                                  fontSize: 40.0,
                                                  fontWeight: FontWeight.w600,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    width: 120.0,
                                    height: 120.0,
                                    child: Icon(
                                      Entypo.bag,
                                      size: 50.0,
                                      color: Colors.white,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                      left: 25.0, top: 12.5, right: 25.0, bottom: 12.5),
                  child: new GridView.count(
                    crossAxisCount: largeScreenGrid ? 2 : 1,
                    childAspectRatio: 1.4,
                    padding: const EdgeInsets.all(02.0),
                    mainAxisSpacing: 20.0,
                    crossAxisSpacing: 20.0,
                    semanticChildCount: 2,
                    shrinkWrap: true,
                    children: [
                      Container(
                        child: Material(
                          elevation: 02.0,
                          borderRadius: BorderRadius.circular(03.0),
                          child: Container(
                            padding: EdgeInsets.all(25.0),
                            child: LineChartSample2(),
                          ),
                        ),
                      ),
                      Container(
                        child: Material(
                          elevation: 02.0,
                          borderRadius: BorderRadius.circular(03.0),
                          child: Container(
                            padding: EdgeInsets.all(02.0),
                            child: Center(
                              child: PieChartSample2(),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                      left: 25.0, top: 12.5, right: 25.0, bottom: 12.5),
                  height: 500,
                  width: double.infinity,
                  child: YoutubePlayerControllerProvider(
                    // Provides controller to all the widget below it.
                    controller: _controller,
                    child: YoutubePlayerIFrame(
                      aspectRatio: 16 / 9,
                    ),
                  ),
                ),
                SizedBox(
                  height: 40,
                )
              ],
            ),
          ),
          // Container(
          //   height: 65.0,
          //   color: Colors.white,
          //   child: Material(
          //     elevation: 2.0,
          //     child: TopBar(),
          //   ),
          // ),
        ],
      ),
    );
  }
}

class SalesData {
  SalesData(this.year, this.sales);
  final String year;
  final double sales;
}
