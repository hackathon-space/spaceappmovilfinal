import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:spaceapp/src/theme/theme.dart';
import 'package:provider/provider.dart';

class MenuWidget extends StatelessWidget {
  final colorbase = Color(0xff01a745);
  // final _estiloTexto = new TextStyle(fontSize: 15, color: Color(0xff01a745));

  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;

    final appTheme = Provider.of<ThemeChanger>(context);
    final accentColor = appTheme.currentTheme.primaryColor;
    final appTheme2 = Provider.of<ThemeChanger>(context).currentTheme;

    return SafeArea(
      child: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Container(),
              decoration: BoxDecoration(
                  color: Color.fromRGBO(0, 0, 0, 1),
                  image: DecorationImage(
                    image: AssetImage('assets/images.png'),
                    //fit: BoxFit.cover
                  )),
            ),
            Container(
              child: ListTile(
                leading: Icon(Icons.map, color: appTheme2.primaryColor),
                title: Text('Location', style: TextStyle(color: accentColor)),
                trailing: Icon(Icons.keyboard_arrow_right,
                    color: appTheme2.primaryColor),
                onTap: () => Navigator.pushReplacementNamed(context, 'Mapa'),
              ),
            ),
            Divider(),
            Container(
              child: ListTile(
                leading:
                    Icon(Icons.bento_rounded, color: appTheme2.primaryColor),
                title: Text('Mapped beaches',
                    style: TextStyle(color: accentColor)),
                trailing: Icon(Icons.keyboard_arrow_right,
                    color: appTheme2.primaryColor),
                onTap: () =>
                    Navigator.pushReplacementNamed(context, 'PlayasMapeadas'),
              ),
            ),
            Divider(),
            Container(
              child: ListTile(
                leading: Icon(Icons.pages, color: appTheme2.primaryColor),
                title: Text('Dashboard', style: TextStyle(color: accentColor)),
                trailing: Icon(Icons.keyboard_arrow_right,
                    color: appTheme2.primaryColor),
                onTap: () =>
                    Navigator.pushReplacementNamed(context, 'Dashboard'),
              ),
            ),
            Divider(),
            if (_screenSize.width >= 700)
              Container(
                child: ListTile(
                  leading: Icon(Icons.book, color: appTheme2.primaryColor),
                  title: Text('About', style: TextStyle(color: accentColor)),
                  trailing: Icon(Icons.keyboard_arrow_right,
                      color: appTheme2.primaryColor),
                  onTap: () => Navigator.pushReplacementNamed(context, 'About'),
                ),
              ),
            if (_screenSize.width >= 700) Divider(),
            Container(
              child: ListTile(
                leading: Icon(Icons.person, color: appTheme2.primaryColor),
                title: Text('User Guide', style: TextStyle(color: accentColor)),
                trailing: Icon(Icons.keyboard_arrow_right,
                    color: appTheme2.primaryColor),
                onTap: () =>
                    Navigator.pushReplacementNamed(context, 'UserGuide'),
              ),
            ),
            Divider(),
            if (_screenSize.width >= 700)
              Container(
                child: ListTile(
                  leading:
                      Icon(Icons.phone_iphone, color: appTheme2.primaryColor),
                  title:
                      Text('Mobile View', style: TextStyle(color: accentColor)),
                  trailing: Icon(Icons.keyboard_arrow_right,
                      color: appTheme2.primaryColor),
                  onTap: () =>
                      Navigator.pushReplacementNamed(context, 'mobileview'),
                ),
              ),
            if (_screenSize.width >= 700) Divider(),
            if (_screenSize.width >= 700)
              Container(
                child: ListTile(
                  leading: Icon(Icons.query_stats_outlined,
                      color: appTheme2.primaryColor),
                  title: Text('Canva', style: TextStyle(color: accentColor)),
                  trailing: Icon(Icons.keyboard_arrow_right,
                      color: appTheme2.primaryColor),
                  onTap: () => Navigator.pushReplacementNamed(context, 'Canva'),
                ),
              ),
            if (_screenSize.width >= 700) Divider(),
            if (_screenSize.width <= 700)
              Container(
                child: ListTile(
                    leading: Icon(Icons.double_arrow_outlined,
                        color: appTheme2.primaryColor),
                    title: Text('Exit', style: TextStyle(color: accentColor)),
                    trailing: Icon(Icons.keyboard_arrow_right,
                        color: appTheme2.primaryColor),
                    onTap: () {
                      SystemChannels.platform
                          .invokeMethod('SystemNavigator.pop');
                    }),
              ),
            if (_screenSize.width <= 700) Divider(),
            SizedBox(height: 20),
            ListTile(
              leading: Icon(Icons.lightbulb_outline, color: accentColor),
              title: Text('Dark Mode', style: TextStyle(color: accentColor)),
              trailing: Switch.adaptive(
                  value: appTheme.darkTheme,
                  activeColor: accentColor,
                  onChanged: (value) => appTheme.darkTheme = value),
            ),
            SafeArea(
              bottom: true,
              top: false,
              left: false,
              right: false,
              child: ListTile(
                leading: Icon(Icons.add_to_home_screen, color: accentColor),
                title:
                    Text('Custom Theme', style: TextStyle(color: accentColor)),
                trailing: Switch.adaptive(
                    value: appTheme.customTheme,
                    activeColor: accentColor,
                    onChanged: (value) => appTheme.customTheme = value),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
