import 'package:flutter/material.dart';
import 'package:spaceapp/src/dashboard/dashboard.dart';
import 'package:spaceapp/src/pages/canva_page.dart';
import 'package:spaceapp/src/pages/envivo_page.dart';
import 'package:spaceapp/src/pages/guide_page.dart';
import 'package:spaceapp/src/pages/mapa.dart';
import 'package:spaceapp/src/pages/punto_id_page.dart';
import 'package:spaceapp/src/pages/puntos_reciclaje.dart';

import 'package:spaceapp/src/pages/splash_screen.dart';
import 'package:spaceapp/src/pages/user_page.dart';
import 'package:spaceapp/src/pages/vsitaCelular_page.dart';

Map<String, WidgetBuilder> getApplicationRoutes() {
  return <String, WidgetBuilder>{
    'splash': (BuildContext context) => SplashScreenPage(),
    'Mapa': (BuildContext context) => MapaPage(),
    'PlayasMapeadas': (BuildContext context) => PuntosReciclajePage(),
    'punto': (BuildContext context) => PuntoIdPage(),
    'CamGalley': (BuildContext context) => EnvivoPage(),
    'Dashboard': (BuildContext context) => MyHomePage(),
    'About': (BuildContext context) => GuidePage(),
    'UserGuide': (BuildContext context) => UserPage(),
    'mobileview': (BuildContext context) => MovilPage(),
    'Canva': (BuildContext context) => CanvaPage(),
  };
}
