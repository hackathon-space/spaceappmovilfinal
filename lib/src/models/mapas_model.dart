// To parse this JSON data, do
//
//     final mapaDataModel = mapaDataModelFromJson(jsonString);

import 'dart:convert';

MapaDataModel mapaDataModelFromJson(String str) =>
    MapaDataModel.fromJson(json.decode(str));

String mapaDataModelToJson(MapaDataModel data) => json.encode(data.toJson());

class MapaDataModel {
  MapaDataModel({
    this.id,
    this.bolsa,
    this.botella,
    this.carton,
    this.cuaderno,
    this.estatico,
    this.fecha,
    this.imagen,
    this.imagencontaminada1,
    this.imagencontaminada2,
    this.imagencontaminada3,
    this.latitud,
    this.longitud,
    this.mascarilla,
    this.nombre,
    this.ubicacion,
  });

  String id;
  int bolsa;
  int botella;
  int carton;
  int cuaderno;
  int estatico;
  DateTime fecha;
  String imagen;
  String imagencontaminada1;
  String imagencontaminada2;
  String imagencontaminada3;
  double latitud;
  double longitud;
  int mascarilla;
  String nombre;
  String ubicacion;

  factory MapaDataModel.fromJson(Map<String, dynamic> json) => MapaDataModel(
        id: json["id"],
        bolsa: json["bolsa"],
        botella: json["botella"],
        carton: json["carton"],
        cuaderno: json["cuaderno"],
        estatico: json["estatico"],
        fecha: DateTime.parse(json["fecha"]),
        imagen: json["imagen"],
        imagencontaminada1: json["imagencontaminada1"],
        imagencontaminada2: json["imagencontaminada2"],
        imagencontaminada3: json["imagencontaminada3"],
        latitud: json["latitud"].toDouble(),
        longitud: json["longitud"].toDouble(),
        mascarilla: json["mascarilla"],
        nombre: json["nombre"],
        ubicacion: json["ubicacion"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "bolsa": bolsa,
        "botella": botella,
        "carton": carton,
        "cuaderno": cuaderno,
        "estatico": estatico,
        "fecha": fecha.toIso8601String(),
        "imagen": imagen,
        "imagencontaminada1": imagencontaminada1,
        "imagencontaminada2": imagencontaminada2,
        "imagencontaminada3": imagencontaminada3,
        "latitud": latitud,
        "longitud": longitud,
        "mascarilla": mascarilla,
        "nombre": nombre,
        "ubicacion": ubicacion,
      };
  Map<String, dynamic> dataDash() => {
        "bolsa": bolsa,
        "botella": botella,
        "carton": carton,
        "cuaderno": cuaderno,
        "mascarilla": mascarilla
      };
}



// DashboardDataModel dashboardDataModelFromJson(String str) => DashboardDataModel.fromJson(json.decode(str));

// String dashboardDataModelToJson(DashboardDataModel data) => json.encode(data.toJson());

// class DashboardDataModel {
//     DashboardDataModel({
//         this.bolsa,
//         this.botella,
//         this.carton,
//         this.cuaderno,
//         this.mascarilla,
//     });

//     int bolsa;
//     int botella;
//     int carton;
//     int cuaderno;
//     int mascarilla;

//     factory DashboardDataModel.fromJson(Map<String, dynamic> json) => DashboardDataModel(
//         bolsa: json["bolsa"],
//         botella: json["botella"],
//         carton: json["carton"],
//         cuaderno: json["cuaderno"],
//         mascarilla: json["mascarilla"],
//     );

//     Map<String, dynamic> toJson() => {
//         "bolsa": bolsa,
//         "botella": botella,
//         "carton": carton,
//         "cuaderno": cuaderno,
//         "mascarilla": mascarilla,
//     };
// }
