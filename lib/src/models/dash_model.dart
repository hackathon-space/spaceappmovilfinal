// To parse this JSON data, do
//
//     final dashboardDataModel = dashboardDataModelFromJson(jsonString);

import 'dart:convert';

DashboardDataModel dashboardDataModelFromJson(String str) => DashboardDataModel.fromJson(json.decode(str));

String dashboardDataModelToJson(DashboardDataModel data) => json.encode(data.toJson());

class DashboardDataModel {
    DashboardDataModel({
        this.bolsa,
        this.botella,
        this.carton,
        this.cuaderno,
        this.mascarilla,
    });

    dynamic bolsa;
    dynamic botella;
    dynamic carton;
    dynamic cuaderno;
    dynamic mascarilla;

    factory DashboardDataModel.fromJson(Map<String, dynamic> json) => DashboardDataModel(
        bolsa: json["bolsa"],
        botella: json["botella"],
        carton: json["carton"],
        cuaderno: json["cuaderno"],
        mascarilla: json["mascarilla"],
    );

    Map<String, dynamic> toJson() => {
        "bolsa": bolsa,
        "botella": botella,
        "carton": carton,
        "cuaderno": cuaderno,
        "mascarilla": mascarilla,
    };
}
