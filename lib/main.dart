import 'dart:ui';

import 'package:spaceapp/src/routes/routes.dart';
import 'package:spaceapp/src/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() => runApp(
    ChangeNotifierProvider(create: (_) => new ThemeChanger(1), child: MyApp()));

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final currentTheme = Provider.of<ThemeChanger>(context).currentTheme;

    return MaterialApp(
        scrollBehavior: MyCustomScrollBehavior(),
        theme: currentTheme,
        debugShowCheckedModeBanner: false,
        title: 'Space App',
        initialRoute: 'splash',
        routes: getApplicationRoutes());
  }
}

class MyCustomScrollBehavior extends MaterialScrollBehavior {
  // Override behavior methods and getters like dragDevices
  @override
  Set<PointerDeviceKind> get dragDevices => {
        PointerDeviceKind.touch,
        PointerDeviceKind.mouse,
        // etc.
      };
}
